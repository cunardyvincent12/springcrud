package com.bncc.RnD.EventApplicant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventApplicantApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventApplicantApplication.class, args);
	}

}
