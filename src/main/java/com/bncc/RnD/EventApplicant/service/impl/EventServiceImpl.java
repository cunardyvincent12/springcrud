package com.bncc.RnD.EventApplicant.service.impl;

import com.bncc.RnD.EventApplicant.domain.Applicant;
import com.bncc.RnD.EventApplicant.dto.mapper.room.ApplicantCreateMapper;
import com.bncc.RnD.EventApplicant.dto.mapper.room.ApplicantUpdateMapper;
import com.bncc.RnD.EventApplicant.dto.model.room.ApplicantCreateDto;
import com.bncc.RnD.EventApplicant.dto.model.room.ApplicantUpdateDto;
import com.bncc.RnD.EventApplicant.repository.EventRepository;
import com.bncc.RnD.EventApplicant.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    EventRepository eventRepository;

    @Override
    public List<Applicant> fetchAllApplicant(){
        return eventRepository.findAll();
    }

    @Override
    public ApplicantCreateDto createApplicant(Applicant applicant){
        eventRepository.save(applicant);

        return ApplicantCreateMapper.convertEntityToDto(applicant);
    }

    @Override
    public ApplicantUpdateDto updateApplicant(Applicant applicant){
        Applicant tempApplicant = eventRepository.findById(applicant.getId()).orElse(null);
        if (tempApplicant == null){
            return null;
        }

        tempApplicant.setNama(applicant.getNama());
        tempApplicant.setEmail(applicant.getEmail());
        tempApplicant.setPhoneNumber(applicant.getPhoneNumber());
        tempApplicant.setOccupancy(applicant.getOccupancy());
        tempApplicant.setDateOfBirth(applicant.getDateOfBirth());
        tempApplicant.setGender(applicant.getGender());
        tempApplicant.setLineId(applicant.getLineId());
        tempApplicant.setNim(applicant.getNim());

        eventRepository.save(tempApplicant);
        return ApplicantUpdateMapper.convertEntityToDto(applicant);
    }

    @Override
    public void deleteApplicant(Long id){
        eventRepository.deleteById(id);
    }
}
