package com.bncc.RnD.EventApplicant.service;

import com.bncc.RnD.EventApplicant.domain.Applicant;
import com.bncc.RnD.EventApplicant.dto.model.room.ApplicantCreateDto;
import com.bncc.RnD.EventApplicant.dto.model.room.ApplicantUpdateDto;

import java.util.List;

public interface EventService {

    public List<Applicant> fetchAllApplicant();
    public ApplicantCreateDto createApplicant(Applicant applicant);
    public ApplicantUpdateDto updateApplicant(Applicant applicant);
    public void deleteApplicant(Long id);
}
