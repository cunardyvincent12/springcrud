package com.bncc.RnD.EventApplicant.repository;

import com.bncc.RnD.EventApplicant.domain.Applicant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<Applicant, Long> {

    @Override
    List<Applicant> findAll();

    Optional<Applicant> findById(long id);

}
