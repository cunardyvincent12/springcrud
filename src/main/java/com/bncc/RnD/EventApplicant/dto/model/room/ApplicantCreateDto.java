package com.bncc.RnD.EventApplicant.dto.model.room;

import com.bncc.RnD.EventApplicant.dto.model.BaseDto;

import java.util.Date;

public class ApplicantCreateDto extends BaseDto {

    private long id;
    private String nama;
    private String email;
    private String phoneNumber;
    private String occupancy;
    private Date dateOfBirth;
    private String gender;
    private String lineId;
    private String nim;


    public ApplicantCreateDto(long id, String nama, String email, String phoneNumber, String occupancy, Date dateOfBirth, String gender, String lineId, String nim) {
        this.id = id;
        this.nama = nama;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.occupancy = occupancy;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.lineId = lineId;
        this.nim = nim;
    }

    public ApplicantCreateDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(String occupancy) {
        this.occupancy = occupancy;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }
}

