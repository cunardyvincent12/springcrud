package com.bncc.RnD.EventApplicant.dto.mapper.room;

import com.bncc.RnD.EventApplicant.domain.Applicant;
import com.bncc.RnD.EventApplicant.dto.model.room.ApplicantUpdateDto;

public class ApplicantUpdateMapper {
    public static ApplicantUpdateDto convertEntityToDto(Applicant applicant){
        return new ApplicantUpdateDto(
                applicant.getId(),
                applicant.getNama(),
                applicant.getEmail(),
                applicant.getPhoneNumber(),
                applicant.getOccupancy(),
                applicant.getDateOfBirth(),
                applicant.getGender(),
                applicant.getLineId(),
                applicant.getNim()
        );
    }
}
