package com.bncc.RnD.EventApplicant.dto.mapper.room;

import com.bncc.RnD.EventApplicant.domain.Applicant;
import com.bncc.RnD.EventApplicant.dto.model.room.ApplicantCreateDto;

public class ApplicantCreateMapper {
    public static ApplicantCreateDto convertEntityToDto(Applicant applicant){
        return new ApplicantCreateDto(
                applicant.getId(),
                applicant.getNama(),
                applicant.getEmail(),
                applicant.getPhoneNumber(),
                applicant.getOccupancy(),
                applicant.getDateOfBirth(),
                applicant.getGender(),
                applicant.getLineId(),
                applicant.getNim()

        );
    }

}
