package com.bncc.RnD.EventApplicant.controller;

import com.bncc.RnD.EventApplicant.domain.Applicant;
import com.bncc.RnD.EventApplicant.dto.model.room.ApplicantCreateDto;
import com.bncc.RnD.EventApplicant.dto.model.room.ApplicantUpdateDto;
import com.bncc.RnD.EventApplicant.service.EventService;
import com.bncc.RnD.EventApplicant.service.impl.EventServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bncc")
public class EventApplicantController {

    @Autowired
    EventService eventService;

    @RequestMapping(method = RequestMethod.GET, value="/view")
    public List<Applicant> fetchAllApplicant(){
        return eventService.fetchAllApplicant();
    }

    @RequestMapping(method = RequestMethod.POST, value="/post")
    public ApplicantCreateDto addApplicant(@RequestBody Applicant applicant){
        return eventService.createApplicant(applicant);
    }

    @RequestMapping(method = RequestMethod.PUT, value="/update")
    public ApplicantUpdateDto updateApplicant(@RequestBody Applicant applicant){
        return eventService.updateApplicant(applicant);

    }
    @RequestMapping(method = RequestMethod.DELETE, value="/delete/{id}")
    public void deleteApplicant(@PathVariable Long id){
        eventService.deleteApplicant(id);
    }


}
